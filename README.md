# Macro

[Logo](Logo)

Macro is a cross-platform mobile application built with Flutter for tracking your dietary macros to help you kickstart and maintain your diet!

# Features
-----------------
- Customize macro percentages
- Create meal-time alerts 
- Visualize your remaining macros
- Find recipes that will fit in to your remaining daily macros
- Android Wear watchface
- More things!

# Contributing
-----------------
Macro is an open source project and we welcome pull requests! If you run into a bug while using Macro, please open up an issue so we can get working on fixing the problem!

We need helpers! If you're familiar with Flutter/Dart or Android Wear please contact @TheNightmanCodeth on Github/Bitbucket, @TheNightman on XDA, or @joooyooo2 on twitter!