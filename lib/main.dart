import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'src/widgets/GraphDetailsList.dart';
import 'src/screens/settings/Settings.dart';
import 'src/widgets/CircleGraphWidget.dart';

void main() => runApp(new MacroApp());

class MacroApp extends StatelessWidget {
  static const String VERSION_NUMBER = "0.0-alpha_1";
  static final ThemeData APP_THEME = ThemeData(
    accentColor: Colors.amberAccent, 
    primaryColor: Colors.amber,
  );
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Macro',
      theme: APP_THEME,
      home: new MacroHome(title: 'Macro'),
    );
  }
}

class MacroHome extends StatefulWidget {
  MacroHome({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MacroHomePageState createState() => new _MacroHomePageState();
}

class _MacroHomePageState extends State<MacroHome> {
  List<Tuple2<Tuple2<String, Color>, Tuple2<int,int>>> _macros 
  = List<Tuple2<Tuple2<String, Color>, Tuple2<int,int>>>();

  @override
  void initState() {
    super.initState();
    loadMacrosFromSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    
    return new Scaffold(
      key: _scaffoldKey,
      drawer: new Drawer(
        child: new Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ListView(
              shrinkWrap: true,
              children: <Widget>[
                DrawerHeader(
                  child: Text(
                    'Macro',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                    ),
                  ),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.blueAccent,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.fitness_center),
                  title: Text('Activity'),
                ),
              ],
            ),
            Expanded(
              child: new Align(
                alignment: FractionalOffset.bottomCenter,
                 // onTap
                child: Material(  
                  elevation: 15.0,                
                  child: InkWell(
                    onTap: () {
                      /* Gotta pop the drawer or else the context will think 
                        it's still open when we come back from a different  
                        page and throw an exception.                        */
                      Navigator.pop(context);
                      Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new Settings(),
                      ));
                    },
                    child: ListTile(
                      leading: Icon(Icons.settings),
                      title: Text('Settings'),                        
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        iconTheme: new IconThemeData(
          color: Colors.black,
        ),
        title: const Text(
          'Macro',
           style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white10,
        elevation: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),
      body: new Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: new Column(
          children: <Widget>[
            new Align(
              alignment: Alignment.center,
              child: new CustomPaint(
                painter: new CircleGraphWidget(_macros),
                //Apparently this needs to be here?
                child: null,
              ),
            ),
            new Expanded(
              child: new Align(
                alignment: FractionalOffset.bottomCenter,
                child: new GraphDetailsList(items: _macros),
              ),
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  } // build

  loadMacrosFromSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> macroNames = prefs.getStringList('prefs_macroNames');
    List<String> macroValue = prefs.getStringList('prefs_macroValue');
    List<String> macroMax   = prefs.getStringList('prefs_macroMax');
    List<String> macroColor = prefs.getStringList('prefs_macroColor');
    
    if (macroNames != null){
      setState(() {
        for (var i = 0; i < macroNames.length; i++) {
          Tuple2<Tuple2<String, Color>, Tuple2<int,int>> value = 
          Tuple2<Tuple2<String, Color>, Tuple2<int,int>>(
            Tuple2<String, Color>(macroNames[i].toString(), Color(int.parse(macroColor[i]))),
            Tuple2<int, int>(int.parse(macroMax[i]), int.parse(macroValue[i]))
          );
          _macros.add(value);
        }
      });
    }
  }

  List<Tuple2<Tuple2<String, Color>, Tuple2<int,int>>> _makeTestMacros() {
    List<Tuple2<Tuple2<String, Color>, Tuple2<int,int>>> macros =  
    List<Tuple2<Tuple2<String, Color>, Tuple2<int,int>>>();
    macros
      .add(Tuple2<Tuple2<String, Color>, Tuple2<int,int>>(
        Tuple2<String,Color>("Fiber", Colors.red[600]),
        Tuple2<int,int>(10,45)));

    macros
      .add(Tuple2<Tuple2<String, Color>, Tuple2<int,int>>(
      Tuple2<String,Color>("Carbs", Colors.amber[800]),
      Tuple2<int,int>(20, 20)));

    macros
      .add(Tuple2<Tuple2<String, Color>, Tuple2<int,int>>(
      Tuple2<String,Color>("Protein", Colors.green[600]),
      Tuple2<int,int>(20,70)));

    macros
      .add(Tuple2<Tuple2<String, Color>, Tuple2<int,int>>(
      Tuple2<String,Color>("Fat", Colors.blue[600]),
      Tuple2<int,int>(20,50)));

    macros
      .add(Tuple2<Tuple2<String, Color>, Tuple2<int,int>>(
      Tuple2<String,Color>("Calories", Colors.yellow[600]),
      Tuple2<int,int>(30,37)));
    return macros;
  }
}
