import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../main.dart';

class MacronutrientSettings extends StatefulWidget {
  MacronutrientSettings({Key key}): super(key: key);

  @override
  _MacronutrientSettingsState createState() => new _MacronutrientSettingsState();
}

class _MacronutrientSettingsState extends State<MacronutrientSettings> {

  List<Tuple2<Tuple2<String, Color>, Tuple2<int,int>>> _macros 
  = List<Tuple2<Tuple2<String, Color>, Tuple2<int,int>>>();

  @override
  void initState() {
    super.initState();
    loadMacrosFromSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(
          color: Colors.black26,
        ),
        title: const Text(
          'Settings',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        elevation: 2.0,
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back,
            color: Colors.black
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: _makeMacroSettings(),
      )
    );
  }

  List<Widget> _makeMacroSettings() {
    return <Widget> [
      Material(
        elevation: 2.0,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
              child: Text(
                'Macros',
                style: TextStyle(
                  color: MacroApp.APP_THEME.accentColor,
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.book),
              title: Text('Macros'),
            ),
            ListView(
              shrinkWrap: true,
              children: List<Widget>.generate(_macros.length, _buildMacroItem),
            ),
            ListTile(
              leading: Icon(Icons.add),
              title: Text('New Macro'),
              onTap: () {
                _addItemToSharedPrefs("test", "75", "25", "0xFF808080");
                setState(() {
                  loadMacrosFromSharedPrefs();
                });
              },
            ),
          ],
        ),
      ),
    ];
  }

  Widget _buildMacroItem(int index) {
    return ListTile(
      leading: Icon(Icons.label, color: _macros[index].item1.item2),
      title: Text(_macros[index].item1.item1),
    );
  }

  loadMacrosFromSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> macroNames = prefs.getStringList('prefs_macroNames');
    List<String> macroValue = prefs.getStringList('prefs_macroValue');
    List<String> macroMax   = prefs.getStringList('prefs_macroMax');
    List<String> macroColor = prefs.getStringList('prefs_macroColor');
      
    if (macroNames != null){
      setState(() {
        for (var i = 0; i < macroNames.length; i++) {
          Tuple2<Tuple2<String, Color>, Tuple2<int,int>> value = 
          Tuple2<Tuple2<String, Color>, Tuple2<int,int>>(
            Tuple2<String, Color>(macroNames[i].toString(), Color(int.parse(macroColor[i]))),
            Tuple2<int, int>(int.parse(macroMax[i]), int.parse(macroValue[i]))
          );
          _macros.add(value);
        }
      });
    }
  }

  _addItemToSharedPrefs(String name, String val, String max, String color) async {
    // Get all the lists from shared_prefs
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> macroNames = prefs.getStringList('prefs_macroNames');
    List<String> macroValue = prefs.getStringList('prefs_macroValue');
    List<String> macroMax   = prefs.getStringList('prefs_macroMax');
    List<String> macroColor = prefs.getStringList('prefs_macroColor');

    //The lists should either all be null, or all not-null. Just to be sure we check them all
    if (macroNames == null || macroValue == null || macroMax == null || macroColor == null) {
      macroNames = List<String>();
      macroValue = List<String>();
      macroMax   = List<String>();
      macroColor = List<String>();
    }

    // Add the values to the lists we got from prefs
    macroNames.add(name);
    macroValue.add(val);
    macroMax.add(max);
    macroColor.add(color);

    // Set the values in the prefs
    await prefs.setStringList('prefs_macroNames', macroNames);
    await prefs.setStringList('prefs_macroValue', macroValue);
    await prefs.setStringList('prefs_macroMax', macroMax);
    await prefs.setStringList('prefs_macroColor', macroColor);
  }
}