User Stories
===========================

### Legend
>> USN(3/6) - User Story Nutrition (userstories about macros and tracking)
>> USR(3/6) - User Story Reminders (userstories about meal reminders)
>> USF(3/6) - User Story Foods     (userstories about custom foods)
>> USS(0/6) - User Story Social    (userstories about social, backup and sync)

##  USN1 - Track Macros

> As a user, I would like to apply custom proportions to my daily macronutrient intake. These macros should include: Carbohydrates, Dietary Fiber, Protein, Fat, Salt/Sodium. These macros should be set to a percentage out of 100.

## USN2 - Preset proportions

> As a user, I would like to be able to select from a set of predefined macro proportions customized for different diets (ie. a Keto preset for proper carb/fat/protein %).

## USN3 - Net Carb tracking

> As a user, I would like the application to track my NET carbs if I toggle an option in the settings. This means that when I input a new meal to the tracker, (IFF the option is enabled in settings) the total dietary fiber should be subtracted from the total carbohydrates.

## USR1 - Meal Alerts

> As a user, I would like to be able to schedule custom alerts for meals to remind me to track my macros for each meal. These should be able to be customized in the settings activity.

## USR2 - Customize Alerts

> As a user, I would like to be able to customize my meal alerts based on my daily eating schedule. I would like to be able to set a custom time for each alert, as well as add custom alerts outside of the 3-a-day default. I would, lastly, like to be able to customize a message for the alert, as well as a 'nag-timer' to specify if/when and how often a reminder should be repeated if I miss it the first time.

## USR3 - Differing schedules

> As a user I would like the ability to modify my eating schedule based on different factors. For instance, If I'm at work, my eating schedule is different than when I'm at home. The app should reflect those changes based on my settings.

## USF1 - Custom Food Entries

> As a user, I would like the ability to add foods I often eat, along with their macro values for easy access when it comes time to add a food to my journal.

## USF2 - Add multiple food items to a meal

> As a user, when adding a new food to my journal, I would like the ability to select more than one saved food item to form a meal. For example, If I have saved 'Peanutbutter', 'Jelly', and 'bread' saved seperately, I would like the ability to add all three to form a pb&j sandwich.

## USF3 - Find recipes matching your remaining macros

> As a user, I would like to press a button to find a list of recipes online that would match the remaining amount of macros I have for the day.

## USS1 - Login to track progress

> As a user, I want to have a Macro account to keep track of my progress accross my devices. When I login, all of my set macros should be restored, along with the custom food items I've created. It should also have a live version of my remaining macros for the day. 

## USS2 - Find friends on Macro

> As a user, I want to find people with similar macro goals, similiar weight loss goals, and/or similar activity goals. These 'friends' should be added to my Macro account, and they should be able to see the activity I consent to sharing in settings. I should, likewise, be able to view their progress.

## USS3 - Share milestones with my friends

> As a user, I would like the ability to share my weight-loss progress with friends on social media, or friends I've found on Macro.